package Interfaces;

import Core.Song;

public interface Iterator {
    boolean hasNext();
    Song next();

}

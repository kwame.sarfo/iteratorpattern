import IterableCollections.PlaylistWithShuffleOff;
import IterableCollections.PlaylistWithShuffleOn;
import Core.Song;
import Interfaces.Iterator;

public class Main {
    public static void main(String[] args) {
        // Downloaded Songs.
        Song[] downloadedSongs = downloadSongs();





        //Playlist with no shuffle iteration.
        PlaylistWithShuffleOff playlist = new PlaylistWithShuffleOff(downloadedSongs);
        Iterator iterator = playlist.iterator();


        System.out.println("Non shuffled Playlist.");
        System.out.println("------------------------");
        while(iterator.hasNext())
        {
            Song currentSong = iterator.next();
            System.out.println(currentSong.getSongName());
        }

        System.out.println('\n');







        //Playlist with shuffle iteration
//        PlaylistWithShuffleOn playlist2 = new PlaylistWithShuffleOn(downloadedSongs);
//        Iterator iterator2 = playlist2.iterator();
//
//
//        System.out.println("Shuffled Playlist");
//        System.out.println("--------------------");
//        while(iterator2.hasNext())
//        {
//            Song currentSong = iterator2.next();
//            System.out.println(currentSong.getSongName());
//        }









//        //Playlist with shuffle iteration and pause
        PlaylistWithShuffleOn playlist2 = new PlaylistWithShuffleOn(downloadedSongs);
        Iterator iterator2 = playlist2.iterator();

        int count =0;

        System.out.println("Shuffled Playlist paused and resumed.");
        System.out.println("--------------------");
        while(iterator2.hasNext() && count < 3)
        {
            Song currentSong = iterator2.next();
            System.out.println(currentSong.getSongName());
            count++;
        }
        System.out.println("Paused.");

        System.out.println('\n');
        System.out.println("Resumed.");
        while(iterator2.hasNext())
        {
            Song currentSong = iterator2.next();
            System.out.println(currentSong.getSongName());

        }



//
//
//        System.out.println();
//
//        List<String> myList = new ArrayList<>();
//        myList.add("String1");
//        myList.add("String2");
//        myList.add("String3");
//
//        java.util.Iterator iterator3 = myList.iterator();
//
//        while(iterator3.hasNext())
//        {
//            String currentString = iterator3.next().toString();
//            System.out.println(currentString);
//        }


    }




















    public static Song[] downloadSongs()
    {
        Song[] songList = new Song[5];
        songList[0] = new Song("song1");
        songList[1] = new Song("song2");
        songList[2] = new Song("song3");
        songList[3] = new Song("song4");
        songList[4] = new Song("song5");

        return songList;
    }
}

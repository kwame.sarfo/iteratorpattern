package Iterators;

import Core.Song;
import Interfaces.Iterator;

public class NoShuffleIterator implements Iterator {
    private Song[] songs;
    private int position;

    public NoShuffleIterator(Song[] songs)
    {
        this.songs = songs;
        this.position = 0;
    }

    @Override
    public Song next()
    {
        Song song = songs[position];
        this.position+=1;
        return song;
    }

    @Override
    public boolean hasNext()
    {
        if(this.position >= this.songs.length || this.songs[this.position] == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

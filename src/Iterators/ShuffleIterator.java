package Iterators;

import Core.Song;
import Interfaces.Iterator;

import java.util.Random;

public class ShuffleIterator implements Iterator {

    private Song[] songs;
    private int position;

    public ShuffleIterator(Song[] songs)
    {
        this.songs = songs;
        Random r = new Random();
        for(int i = this.songs.length-1; i>=0; i--)
        {
            int j = r.nextInt(i+1);

            Song temp = this.songs[i];
            this.songs[i] = this.songs[j];
            this.songs[j] = temp;

        }
        this.position = 0;
    }

    @Override
    public Song next()
    {
        Song song = songs[position];
        this.position+=1;
        return song;
    }

    @Override
    public boolean hasNext()
    {
        if(this.position >= this.songs.length || this.songs[this.position] == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

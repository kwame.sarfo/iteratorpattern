package IterableCollections;

import Core.Song;
import Interfaces.Iterator;
import Interfaces.SongCollection;
import Iterators.ShuffleIterator;

public class PlaylistWithShuffleOn implements SongCollection {
    private Song[] songsOnPlaylist;

    public PlaylistWithShuffleOn(Song[] songs)
    {
        this.songsOnPlaylist = songs;
    }

    public Iterator iterator()
    {
        return new ShuffleIterator(this.songsOnPlaylist);
    }
}

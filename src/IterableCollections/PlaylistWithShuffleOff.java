package IterableCollections;

import Core.Song;
import Interfaces.Iterator;
import Interfaces.SongCollection;
import Iterators.NoShuffleIterator;

public class PlaylistWithShuffleOff implements SongCollection {
    private Song[] songsOnPlaylist;

    public PlaylistWithShuffleOff(Song[] songs)
    {
        this.songsOnPlaylist = songs;
    }

    public Iterator iterator()
    {
        return new NoShuffleIterator(this.songsOnPlaylist);
    }
}
